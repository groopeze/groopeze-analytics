<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Administrator::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        // 'api_token' => char(60)->nullable(),
    ];
});

$factory->state(App\Administrator::class, 'superuser',[
    'name' => 'admin',
    'email' => 'admin@groopeze.com'
]);

$factory->state(App\User::class, 'chris',[
    'name' => 'chris',
    'email' => 'chris@groopeze.com'
]);

