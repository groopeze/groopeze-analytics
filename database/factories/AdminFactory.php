<?php

use Faker\Generator as Faker;

$factory->define(App\Administrator::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'created_at' => date_create()
    ];
});

$factory->state(App\Administrator::class, 'superuser',[
    'name' => 'admin',
    'email' => 'admin@groopeze.com'
]);

$factory->state(App\Administrator::class, 'chris',[
    'name' => 'chris',
    'email' => 'chris@groopeze.com'
]);
