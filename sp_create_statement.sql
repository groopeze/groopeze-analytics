DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_activatedByDateQuery`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
select DATE(datetime_activated) as XAxis, count(id) as YAxis from users where(datetime_activated >= startdate && datetime_activated <= enddate && active > 0 && datetime_activated > 0000-00-00) group by Date(datetime_activated);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_avgSpendPerUser`()
BEGIN
SELECT TRUNCATE(Avg(payments.amount),2) AS Avg_Spend FROM payments
        WHERE (((payments.status)="OK : 0000") && ((payments.amount)<=237 && (payments.amount)>10));
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_createdBookingQuery`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
select DATE(date_booked) as XAxis, count(id) as YAxis from bookings where(date_booked >= startdate && date_booked <= enddate && hide <1 ) group by Date(date_booked);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_createdQuery`(
	IN `startdate` DATE,
	IN `enddate` DATE


)
BEGIN
select DATE(created) as XAxis, count(id) as YAxis from users where(created >= startdate && created <= enddate) group by Date(created);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_get_repeat_users`()
BEGIN
select vwView1.id, vwView1.First_Name, vwView1.Last_Name, vwView1.email, vwView1.cnt, SUM(payments.amount) as total_spend FROM vwView1
inner join payments
on 
payments.paid_by = vwView1.id
group by vwView1.id
order by cnt desc;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_Most_Pop_Days_User_Creation_By_Date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT DATE_FORMAT(created, "%a") AS wd, COUNT(*) AS cnt FROM users WHERE users.created > startdate
        && users.created < enddate GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_Most_Pop_Months_User_Creation_By_Date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT DATE_FORMAT(created, "%b") AS wd, COUNT(*) AS cnt FROM users WHERE users.created > startdate
        && users.created < enddate GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_most_popular_activities_by_bookings_and_spending_INCOMPLETE`()
BEGIN
SELECT 
    location.location,
    day_activity.activity,
    COUNT(day_activity.activity) AS freq,
    SUM(payments.amount) AS total_paid
FROM
    bookings
        INNER JOIN
    location ON location.id = bookings.location_id
        INNER JOIN
    day_activity ON day_activity.b_id = bookings.id
        INNER JOIN
    rsvp ON bookings.id = rsvp.b_id
        INNER JOIN
    payments ON payments.u_id = rsvp.u_id
WHERE
    bookings.hide < 1
        && payments.status = 'OK : 0000'
GROUP BY location.location , day_activity.activity;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_most_Popular_Booking_Day_By_Date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    DATE_FORMAT(date_booked, '%a') AS wd, COUNT(*) AS cnt
FROM
    bookings
WHERE
    hide < 1
        && bookings.date_booked > startdate
        && bookings.date_booked < enddate
GROUP BY wd
ORDER BY cnt DESC;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_most_Popular_Booking_Month_By_Date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    DATE_FORMAT(date_booked, '%b') AS wd, COUNT(*) AS cnt
FROM
    bookings
WHERE
    hide < 1
        && bookings.date_booked > startdate
        && bookings.date_booked < enddate
GROUP BY wd
ORDER BY cnt DESC;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_most_popular_locations`()
BEGIN
SELECT 
    location.location, COUNT(*) AS freq
FROM
    bookings
        INNER JOIN
    location ON location.id = bookings.location_id
WHERE
    hide < 1
GROUP BY location_id;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_most_Popular_Start_Days_By_Date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT DATE_FORMAT(start_date, "%a") AS wd, COUNT(*) AS cnt FROM bookings WHERE hide < 1 && bookings.start_date > startdate
        && bookings.start_date < enddate GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_most_Popular_Start_Months_By_Date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT DATE_FORMAT(start_date, "%b") AS wd, COUNT(*) AS cnt FROM bookings WHERE hide < 1 && bookings.start_date > startdate
        && bookings.start_date < enddate GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_mostPopBookingDays`()
BEGIN
SELECT DATE_FORMAT(date_booked, "%a") AS wd, COUNT(*) AS cnt FROM bookings WHERE hide < 1 GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_mostPopBookingMonths`()
BEGIN
SELECT DATE_FORMAT(date_booked, "%b") AS wd, COUNT(*) AS cnt FROM bookings WHERE hide < 1 GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_mostPopDays`()
BEGIN
SELECT DATE_FORMAT(created, "%a") AS wd, COUNT(*) AS cnt FROM users GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_mostPopMonths`()
BEGIN
SELECT DATE_FORMAT(created, "%b") AS wd, COUNT(*) AS cnt FROM users GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_mostPopStartDays`()
BEGIN
SELECT DATE_FORMAT(start_date, "%a") AS wd, COUNT(*) AS cnt FROM bookings WHERE hide < 1 GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_mostPopStartMonths`()
BEGIN
SELECT DATE_FORMAT(start_date, "%b") AS wd, COUNT(*) AS cnt FROM bookings WHERE hide < 1 GROUP BY wd ORDER BY cnt DESC;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_repeatUsers`()
BEGIN
SELECT users.f_name AS First_Name, users.l_name AS Last_Name, users.email, t.cnt AS No_Of_Repeats, p.total_spend FROM users
        INNER JOIN
        (select payments.u_id, SUM(payments.amount) as total_spend from payments group by u_id) p
        on 
        p.u_id = users.id
        inner join
        (SELECT user_to_bookings.u_id, 
        Count(*) AS cnt FROM user_to_bookings 
        where user_to_bookings.attending = 1
        GROUP BY user_to_bookings.u_id) t 
        ON t.u_id = users.id 
        WHERE users.is_superuser < 1 && users.datetime_activated > "0000-00-00 00:00:00" && users.email NOT LIKE "shanedara@gmail.com"  && users.email NOT LIKE "darraghkirby@facebook.com"  && users.email NOT LIKE "darraghkirby@gmail.com" && users.email NOT LIKE "team@thestagsballs.com"
        ORDER BY No_Of_Repeats desc;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_startBookingQuery`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
select DATE(start_date) as XAxis, count(id) as YAxis from bookings where(start_date >= startdate && start_date <= enddate && hide < 1) group by Date(start_date);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_accounts`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT COUNT(*) as total_accounts from users 
where  
		users.created > startdate && users.created < enddate 
		&& id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_accounts_by_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT COUNT(*) as total_accounts from users 
where users.created > startdate && users.created < enddate 
		&& id NOT IN (
        20028 , 
        19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_accounts_never_paid_for_themsleves_by_create_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_never_paid_for_self
FROM
    payments
        INNER JOIN
    users ON payments.u_id = users.id
WHERE
    u_id NOT IN (SELECT 
            u_id
        FROM
            payments
        WHERE
            u_id = paid_by)
        AND u_id != paid_by
        AND users.created >= startdate
        AND users.created <= enddate
        AND payments.u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_accounts_never_paid_for_themsleves_by_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_never_paid_for_self
FROM
    payments
        INNER JOIN
    users ON payments.u_id = users.id
WHERE
    u_id NOT IN (SELECT 
            u_id
        FROM
            payments
        WHERE
            u_id = paid_by)
        AND u_id != paid_by
        AND users.created >= startdate
        AND users.created <= enddate
        AND payments.u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_accounts_that_have_made_payments`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (payments.u_id)) AS total_with_Payments
FROM
    users
        INNER JOIN
    payments ON users.id = payments.u_id
WHERE
	users.created > startdate && users.created < enddate
		&& payments.amount > 0
        && payments.status = 'OK : 0000'
        && users.id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1); 
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_active_declined`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (users.id)) AS activated_and_declined
FROM
    users
        INNER JOIN
    user_to_bookings ON users.id = user_to_bookings.u_id
        INNER JOIN
    bookings ON user_to_bookings.b_id = bookings.id
WHERE
    bookings.start_date > startdate
        && bookings.start_date < enddate
        && user_to_bookings.attending = 2
        && users.active = 1
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_active_users_never_paid_themselves`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_never_paid_for_self
FROM
    payments
WHERE
    payments.paid_by != payments.u_id
        && payments.status = 'OK : 0000'
        && payments.u_id NOT IN (SELECT 
            payments.u_id
        FROM
            payments
        WHERE
            payments.paid_by = payments.u_id)
        && payments.u_id IN (SELECT 
            users.id
        FROM
            users
        WHERE
            users.datetime_activated > startdate
                && users.datetime_activated < enddate
                && users.active = 1
                AND users.id NOT IN (20028 , 19740,
                17853,
                8730,
                5075,
                4128,
                4127,
                3989,
                3550,
                3419,
                1075,
                412,
                115,
                33,
                2,
                1));
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_active_users_never_paid_themselves_by_create_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_never_paid_for_self
FROM
    payments
WHERE
    payments.paid_by != payments.u_id
        && payments.status = 'OK : 0000'
        && payments.u_id NOT IN (SELECT 
            payments.u_id
        FROM
            payments
        WHERE
            payments.paid_by = payments.u_id)
        && payments.u_id IN (SELECT 
            users.id
        FROM
            users
        WHERE
				users.created > startdate
                && users.created < enddate
                && users.active = 1
                AND users.id NOT IN (20028 , 19740,
                17853,
                8730,
                5075,
                4128,
                4127,
                3989,
                3550,
                3419,
                1075,
                412,
                115,
                33,
                2,
                1));
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_active_users_paid_themselves_by_create_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_paid_for_self
FROM
    payments
WHERE
    payments.paid_by = payments.u_id
        && payments.status = 'OK : 0000'
        && payments.u_id IN (SELECT 
            users.id
        FROM
            users
        WHERE
				users.created > startdate
                && users.created < enddate
                && users.active = 1
                AND users.id NOT IN (20028 , 19740,
                17853,
                8730,
                5075,
                4128,
                4127,
                3989,
                3550,
                3419,
                1075,
                412,
                115,
                33,
                2,
                1));
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_active_users_sent_on_bookings`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_on_bookings
FROM
    user_to_bookings
        INNER JOIN
    users ON user_to_bookings.u_id = users.id
WHERE
    users.datetime_activated > startdate
        && users.datetime_activated < enddate
        && attending = 1
        && users.active = 1
        AND users.id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_active_users_with_payment`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_with_Payments
FROM
    users
        INNER JOIN
    payments ON users.id = payments.u_id
WHERE   
		users.datetime_activated > startdate
        && users.datetime_activated < enddate
		&& payments.status = 'OK : 0000'
        && users.active = 1
        AND users.id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_declined_bookings`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_declined
FROM
    user_to_bookings
INNER JOIN 
bookings
ON
bookings.id = user_to_bookings.b_id
WHERE
	bookings.hide = 0 
	&& bookings.start_date > startdate
	&& bookings.start_date < enddate
    && attending = 2
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_inactive_declined`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(*) AS not_activated_and_declined
FROM
    users
        INNER JOIN
    user_to_bookings ON users.id = user_to_bookings.u_id
        INNER JOIN
    bookings ON user_to_bookings.b_id = bookings.id
WHERE
    bookings.start_date > startdate
        && bookings.start_date < enddate
        && user_to_bookings.attending = 2
        && users.active = 0
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_inactive_users`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT COUNT(*) as total_accounts from users where users.created >= startdate AND users.created <= enddate AND active = 0 AND users.id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_inactive_users_all_time`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT COUNT(*) as total_accounts from users where users.created >= '2013-01-01' AND users.created <= '2017-12-31' AND active = 0 AND users.id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_inactive_users_with_payments_made`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT COUNT(distinct(u_id)) as total_with_payments from users 
        inner join
        payments
        on 
        payments.u_id = users.id
        where users.created >= startdate AND users.created <= enddate AND users.active = 0 AND payments.status = "OK : 0000" AND users.id NOT IN(20028, 19740, 17853, 8730, 5075, 4128, 4127, 3989, 3550, 3419, 1075, 412, 115, 33, 2, 1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_inactive_users_with_payments_made_by_payment_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT COUNT(distinct(u_id)) as total_with_payments from users 
        inner join
        payments
        on 
        payments.u_id = users.id
        where payments.datetime_paid >= startdate AND payments.datetime_paid <= enddate AND users.active = 0 AND payments.status = "OK : 0000" AND users.id NOT IN(20028, 19740, 17853, 8730, 5075, 4128, 4127, 3989, 3550, 3419, 1075, 412, 115, 33, 2, 1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_income_all_time`()
BEGIN
SELECT concat("$", format(sum(payments.amount),2)) as total_amount from payments;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_never_paid_for_themselves`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) as total_never_paid_for_self
FROM
    payments
WHERE
    u_id != paid_by
        AND datetime_paid >= startdate
        AND datetime_paid <= enddate
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_paid_for_themselves`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) as total_paid_for_self
FROM
    payments
WHERE
    u_id = paid_by
        AND datetime_paid >= startdate
        AND datetime_paid <= enddate
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_paid_for_themselves_by_create_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) as total_paid_for_self
FROM
    payments
INNER JOIN 
users
on
users.id = payments.u_id
WHERE
	
		u_id = paid_by
        AND payments.status = 'OK : 0000'
        AND users.created >= startdate
        AND users.created <= enddate
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_paid_for_themselves_by_pay_date`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) as total_paid_for_self
FROM
    payments
WHERE
    u_id = paid_by
        AND datetime_paid >= startdate
        AND datetime_paid <= enddate
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_payments_all_time`()
BEGIN
SELECT count(*) as cnt from payments where payments.amount > 1;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_total_sent_on_bookings`(
IN `startdate` DATE,
IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(DISTINCT (u_id)) AS total_on_bookings
FROM
    user_to_bookings
inner join 
bookings
on 
bookings.id = user_to_bookings.b_id
WHERE
	bookings.start_date > startdate && bookings.start_date < enddate 
    && attending = 1 && attending != 0
        AND u_id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`homestead`@`%` PROCEDURE `sp_totalActiveUsers`(
	IN `startdate` DATE,
	IN `enddate` DATE
)
BEGIN
SELECT 
    COUNT(id) AS TotalActive
FROM
    users
WHERE
    active > 0
		AND users.datetime_activated > startdate
        AND users.datetime_activated < enddate
        AND users.id NOT IN (20028 , 19740,
        17853,
        8730,
        5075,
        4128,
        4127,
        3989,
        3550,
        3419,
        1075,
        412,
        115,
        33,
        2,
        1);END$$
DELIMITER ;
