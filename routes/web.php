<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dbSwap', 'UserController@dbSwap');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users', 'HomeController@users')->name('users');

Route::get('/bookings', 'HomeController@bookings')->name('bookings');

Route::get('/payments', 'HomeController@payments')->name('payments');


Route::get('/users/totalActive', 'UserDataController@totalActive')->name('totalActive');

Route::get('/users/createdActivated', 'UserDataController@createdActivated');

Route::get('/users/mostPopularDays', 'UserDataController@mostPopularDays');

Route::get('/users/repeatUsers', 'UserDataController@repeatUsers');

Route::get('/users/avgSpendPerUser', 'UserDataController@avgSpendPerUser');

Route::get('/users/userPieChart', 'UserDataController@userPieChart');

Route::get('/users/activeUserPieChart', 'UserDataController@activatedUserPieChart');

Route::get('/users/activatedUserByPaymentDatePieChart', 'UserDataController@activatedUserByPaymentDatePieChart');

Route::get('/users/usersWithPaymentsPieChart', 'UserDataController@usersWithPaymentsPieChart');

Route::get('/users/usersWithPaymentsByPaymentDatePieChart', 'UserDataController@usersWithPaymentsByPaymentDatePieChart');

Route::get('/users/nonActivatedUsersWithPaymentsPieChart', 'UserDataController@nonActivatedUsersWithPaymentsPieChart');

Route::get('/users/nonActivatedUsersWithPaymentsByPaymentDatePieChart', 'UserDataController@nonActivatedUsersWithPaymentsByPaymentDatePieChart');

Route::get('/users/usersWhoHaveNeverPaidForThemselves', 'UserDataController@usersWhoHaveNeverPaidForThemselves');

Route::get('/users/usersWhoHaveNeverPaidForThemselvesByPaymentDate', 'UserDataController@usersWhoHaveNeverPaidForThemselvesByPaymentDate');

Route::get('/users/activatedUsersWhoHaveNeverPaidForThemselves', 'UserDataController@activatedUsersWhoHaveNeverPaidForThemselves');

Route::get('/users/activatedUsersWhoHaveNeverPaidForThemselvesByPaymentDate', 'UserDataController@activatedUsersWhoHaveNeverPaidForThemselvesByPaymentDate');


Route::get('/users/totalAccountsAndTotalSentOnBooking', 'UserDataController@totalAccountsAndTotalSentOnBooking');

Route::get('/users/activatedAndNonActivatedAccountsDeclinedATrip', 'UserDataController@activatedAndNonActivatedAccountsDeclinedATrip');

Route::get('/bookings/index', 'BookingsDataController@index');

Route::get('/bookings/repeat-users', 'BookingsDataController@repeatUsers');

Route::get('/bookings/inout', 'BookingsDataController@bookingsInOut');

Route::get('/bookings/mostPopularBookingDays', 'BookingsDataController@mostPopularBookingDays');

Route::get('/bookings/mostPopularStartDays', 'BookingsDataController@mostPopularStartDays');

Route::get('/bookings/incomeStats', 'BookingsDataController@incomeStats');

Route::get('/bookings/incomeOverTime', 'BookingsDataController@incomeOverTime');

Route::get('/bookings/mostPopularLocations', 'BookingsDataController@mostPopularLocations');


Route::get('/bookings/createdActivated', 'BookingsDataController@bookingsCreatedActivated');

Route::get('/landing', 'HomeController@dash')->name('landing');
//REMOVE BELOW, TESTING ONLY
Route::get('/temp', 'UserDataController@thing')->name('temp');