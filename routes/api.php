<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group (['middleware'=>['auth:api']], function () {



// Route::get('/users/totalActive', 'UserDataController@totalActive');

// Route::get('/users/createdActivated', 'UserDataController@createdActivated');

// Route::get('/users/mostPopularDays', 'UserDataController@mostPopularDays');

// Route::get('/users/repeatUsers', 'UserDataController@repeatUsers');

// Route::get('/users/avgSpendPerUser', 'UserDataController@avgSpendPerUser');

// Route::get('/users/userPieChart', 'UserDataController@userPieChart');

// Route::get('/users/activeUserPieChart', 'UserDataController@activatedUserPieChart');

// Route::get('/users/usersWithPaymentsPieChart', 'UserDataController@usersWithPaymentsPieChart');

// Route::get('/users/nonActivatedUsersWithPaymentsPieChart', 'UserDataController@nonActivatedUsersWithPaymentsPieChart');

// Route::get('/users/usersWhoHaveNeverPaidForThemselves', 'UserDataController@usersWhoHaveNeverPaidForThemselves');

// Route::get('/users/activatedUsersWhoHaveNeverPaidForThemselves', 'UserDataController@activatedUsersWhoHaveNeverPaidForThemselves');

// Route::get('/users/totalAccountsAndTotalSentOnBooking', 'UserDataController@totalAccountsAndTotalSentOnBooking');

// Route::get('/users/activatedAndNonActivatedAccountsDeclinedATrip', 'UserDataController@activatedAndNonActivatedAccountsDeclinedATrip');

// Route::get('/bookings/index', 'BookingsDataController@index');

// Route::get('/bookings/repeat-users', 'BookingsDataController@repeatUsers');

// Route::get('/bookings/inout', 'BookingsDataController@bookingsInOut');

// Route::get('/bookings/mostPopularBookingDays', 'BookingsDataController@mostPopularBookingDays');

// Route::get('/bookings/mostPopularStartDays', 'BookingsDataController@mostPopularStartDays');

// Route::get('/bookings/incomeStats', 'BookingsDataController@incomeStats');

// Route::get('/bookings/incomeOverTime', 'BookingsDataController@incomeOverTime');

// Route::get('/bookings/mostPopularLocations', 'BookingsDataController@mostPopularLocations');
// });

//Route::get('/users/created-activated', 'UsersCreatedActivated@createdActivated');