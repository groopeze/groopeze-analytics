<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class LoginAuth
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        $data = $request->input();
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'id'=> [1,2]])) {
            dd($request->user());
            //return redirect()->intended('dashboard');
            return $next($request);
        }
    }
}