<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DataParser;

class UserDataController extends Controller
{
    
    private $dataParser;
    private $db;


    public function __construct(DataParser $dataParser, Request $request)
    {
        $data = $request->all();
        $this->db = $data['selected_db'];
        $this->dataParser = $dataParser;
        //$this->middleware('auth');
    }

    
    
    public function thing(Request $request){
        dd(config('database.default'));       
    }
    

    public function totalActive(Request $request)
    {                
        $totalActiveUsers =  DB::connection($this->db)->select('select count(id) from users as TotalActive where active > 0 && is_superuser < 1');

        return response()->json(['type'=>'info', 'element'=>'noActiveUsers', 'items'=> $totalActiveUsers[0]]); 
    }


    public function createdActivated(Request $request)
    {       
        $startdate = $request->input('dateStart');
        $enddate = $request->input('dateEnd');

        if ($startdate == null || $enddate == null) {
            $startdate = "2017-06-01";//date("Y-m-d") - 60;
            $enddate =  "2017-06-20";//date("Y-m-d") - 30;
        }

        $created = $this->dataParser->dataSeperatorXAxisAndYAxis((DB::connection($this->db)->select('Call sp_createdQuery(?,?)', array($startdate, $enddate))));

        //the below returns the date activated
        $activatedByDate = $this->dataParser->dataSeperatorXAxisAndYAxis((DB::connection($this->db)->select('Call sp_activatedByDateQuery(?,?)', array($startdate, $enddate))));
        $annoyingArray = [$created, $activatedByDate];
        $labelArray = ['Users Created', 'Users Activated'];
        $colorArray = ['rgba(75,192,192,0.4)', 'rgba(255,0,0,0.4)'];
        
        return response()->json(['type'=>'line', 'element'=>'usersCreated', 'labels' => $labelArray, 'colours' => $colorArray, 'items' => $annoyingArray]);//'created' => $created, 'activated'=>$activated, 'activatedByDate'=> $activatedByDate]);
        //return  json_encode(['type'=>'line', 'element'=>'usersCreated', 'items' =>$created]);
    }

    public function mostPopularDays(Request $request)
    {

        $startDateUsers = $request->input('dateStart');
        $endDateUsers = $request->input('dateEnd');
        //$dataFormat = $request->value;
        $dataFormat = ($request->value == null) ? 1 : $request->value;
        
        $mostPop = array();
        if ($dataFormat == 1) {
            $mostPop = DB::connection($this->db)->select('Call sp_Most_Pop_Days_User_Creation_By_Date(?,?);', array($startDateUsers, $endDateUsers));
        }elseif($dataFormat == 2){
            $mostPop = DB::connection($this->db)->select('Call sp_Most_Pop_Months_User_Creation_By_Date(?,?)', array($startDateUsers, $endDateUsers));
        }

        $day = collect($mostPop)->pluck("wd");
        $number = collect($mostPop)->pluck("cnt");
        $labelArray = ['Users Created'];

        $data = array($day,$number);
        return response()->json(['type'=>'bar', 'labels' => $labelArray, 'element'=>'busiestDaysUsers','items' => $data]);
    }

    public function repeatUsers(Request $request)
    { 
        $repeatUsers = DB::connection($this->db)->select('CALL sp_repeatUsers;');

        // $repeatUsers = DB::connection($this->db)->select('SELECT users.f_name AS First_Name, users.l_name AS Last_Name, users.email, t.cnt AS No_Of_Repeats FROM users
        // INNER JOIN
        // (SELECT user_to_bookings.u_id, 
        // Count(*) AS cnt FROM user_to_bookings 
        // GROUP BY u_id 
        // HAVING cnt > 1) t 
        // ON t.u_id = users.id 
        // WHERE users.is_superuser < 1 && users.datetime_activated > "0000-00-00 00:00:00" && users.email NOT LIKE "shanedara@gmail.com"  && users.email NOT LIKE "darraghkirby@facebook.com"  && users.email NOT LIKE "darraghkirby@gmail.com" && users.email NOT LIKE "team@thestagsballs.com"
        // ORDER BY No_Of_Repeats desc;');        
        return response()->json(['type'=>'table', 'labels' => '', 'element'=>'repeatUsersTable', 'items'=>$repeatUsers]);
    }

    public function avgSpendPerUser(Request $request)
    {
        
        $avgSpend = DB::connection($this->db)->select('CALL sp_avgSpendPerUser');

        // $avgSpend = DB::connection($this->db)->select('SELECT TRUNCATE(Avg(payments.amount),2) AS Avg_Spend FROM payments
        // WHERE (((payments.status)="OK : 0000") && ((payments.amount)<=237 && (payments.amount)>10))');

        return response()->json(['type'=>'info', 'label'=> '', 'element'=>'avgUserSpend', 'items'=> $avgSpend[0]]);
    }

    public function totalAccountsAndTotalSentOnBooking(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        // if ($startDatePie == null || $endDatePie == null) {
        //     $startDatePie = "2013-12-16";//date("Y-m-d") - 60;
        //     $endDatePie =  "2017-12-31";//date("Y-m-d") - 30;
        // }

        $totalSentOnStags = DB::connection($this->db)->select('CALL sp_total_sent_on_bookings(?,?);', array($startDatePie, $endDatePie));
        $totalDeclined = DB::connection($this->db)->select('CALL sp_total_declined_bookings(?,?);', array($startDatePie, $endDatePie));
        $totalAccounts = DB::connection($this->db)->select('CALL sp_total_accounts_by_created(?,?);', array($startDatePie, $endDatePie));

        $pieArray=[$totalSentOnStags[0]->total_on_bookings, $totalDeclined[0]->total_declined, $totalAccounts[0]->total_accounts - $totalSentOnStags[0]->total_on_bookings - $totalDeclined[0]->total_declined];
        //$pieArray= [$startDatePie, $endDatePie];
        return response()->json(['type'=>'pie', 'label'=>['Total sent on stags','total declined', 'Total not gone on booking'],'element'=> 'totalAccountsAndTotalSentOnBookingPieChart','items'=>$pieArray]);
    }

    public function activatedAndNonActivatedAccountsDeclinedATrip(Request $request)
    {
        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalNotActivatedAndDeclined = DB::connection($this->db)->select('CALL sp_total_inactive_declined(?,?);', array($startDatePie, $endDatePie));

        $totalActivatedAndDeclined = DB::connection($this->db)->select('CALL sp_total_active_declined(?,?);', array($startDatePie, $endDatePie));

        $pieArray=[$totalNotActivatedAndDeclined[0]->not_activated_and_declined, $totalActivatedAndDeclined[0]->activated_and_declined];
        return response()->json(['type'=>'pie', 'label'=>['Total not activated and declined', 'Total activated and declined'],'element'=> 'activatedAndActivatedDeclinedBooking','items'=>$pieArray]);

    }

    public function userPieChart(Request $request)
    {
        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalUsers = DB::connection($this->db)->select('CALL sp_total_accounts(?,?);', array($startDatePie, $endDatePie));

        $totalActivated = DB::connection($this->db)->select('Call sp_total_active_users_by_creation_date(?,?);', array($startDatePie, $endDatePie));
        //dd($totalActivated);
        $pieArray=[$totalUsers[0]->total_accounts - $totalActivated[0]->TotalActive, $totalActivated[0]->TotalActive];
        return response()->json(['type'=>'pie', 'label'=>['Total Not Activated', 'Total Activated'],'element'=> 'totalUsersVsActivated','items'=>$pieArray]);
    }

    public function usersWithPaymentsPieChart(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalUsers = DB::connection($this->db)->select('CALL sp_total_accounts(?,?);', array($startDatePie, $endDatePie));

        $totalWithPayments = DB::connection($this->db)->select('CALL sp_total_accounts_that_have_made_payments(?,?);', array($startDatePie, $endDatePie));
        //dd($totalUsers);
        $pieArray=[$totalUsers[0]->total_accounts-$totalWithPayments[0]->total_with_Payments, $totalWithPayments[0]->total_with_Payments];
        return response()->json(['type'=>'pie', 'label'=>['Users With No Payment', 'Users With Payment'],'element'=> 'totalUsersAndUsersWithPaymentPie','items'=>$pieArray]);
    }


    public function usersWithPaymentsByPaymentDatePieChart(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalUsers = DB::connection($this->db)->select('CALL sp_total_accounts(?,?);', array($startDatePie, $endDatePie));

        $totalWithPayments = DB::connection($this->db)->select('CALL sp_total_accounts_made_payments_by_payment_date(?,?);', array($startDatePie, $endDatePie));
        //dd($totalUsers);
        $pieArray=[$totalUsers[0]->total_accounts-$totalWithPayments[0]->total_with_Payments, $totalWithPayments[0]->total_with_Payments];
        return response()->json(['type'=>'pie', 'label'=>['Users With No Payment', 'Users With Payment'],'element'=> 'totalUsersAndUsersWithPaymentByPaymentDatePie','items'=>$pieArray]);
    }


    public function activatedUserPieChart(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalActiveUsers = DB::connection($this->db)->select('CALL sp_totalActiveUsers(?,?);', array($startDatePie, $endDatePie));

        $totalWithPayments = DB::connection($this->db)->select('CALL sp_total_active_users_with_payment(?,?);', array($startDatePie, $endDatePie));

        $pieArray=[$totalActiveUsers[0]->TotalActive - $totalWithPayments[0]->total_with_Payments, $totalWithPayments[0]->total_with_Payments];
        return response()->json(['type'=>'pie', 'label'=>['Active users with no payment', 'Active Users With Payment'],'element'=> 'totalActiveUsersAndUsersWithPaymentPie','items'=>$pieArray]);
    }

    public function activatedUserByPaymentDatePieChart(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalActiveUsers = DB::connection($this->db)->select('CALL sp_totalActiveUsers(?,?);', array($startDatePie, $endDatePie));

        $totalWithPayments = DB::connection($this->db)->select('CALL sp_total_active_users_with_payment_by_payment_date(?,?);', array($startDatePie, $endDatePie));

        $pieArray=[$totalActiveUsers[0]->TotalActive - $totalWithPayments[0]->total_with_Payments, $totalWithPayments[0]->total_with_Payments];
        return response()->json(['type'=>'pie', 'label'=>['Active users with no payment', 'Active Users With Payment'],'element'=> 'totalActiveUsersAndUsersWithPaymentByPaymentDatePie','items'=>$pieArray]);
    }

    public function nonActivatedUsersWithPaymentsPieChart(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalNonActiveUsers = DB::connection($this->db)->select('CALL sp_total_inactive_users(?,?);', array($startDatePie, $endDatePie));

        $totalWithPayments = DB::connection($this->db)->select('CALL sp_total_inactive_users_with_payments_made(?,?);', array($startDatePie, $endDatePie));
        //dd($totalUsers);
        $pieArray=[$totalNonActiveUsers[0]->total_accounts - $totalWithPayments[0]->total_inactive_with_payments, $totalWithPayments[0]->total_inactive_with_payments];
        return response()->json(['type'=>'pie', 'label'=>['Non-active, no payment', 'Non-Active Users With Payment'],'element'=> 'nonActivatedUsersWithPaymentsPieChart','items'=>$pieArray]);
    }

    public function nonActivatedUsersWithPaymentsByPaymentDatePieChart(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalNonActiveUsers = DB::connection($this->db)->select('CALL sp_total_inactive_users(?,?);', array($startDatePie, $endDatePie));

        $totalWithPayments = DB::connection($this->db)->select('CALL sp_total_inactive_users_with_payments_made_by_payment_date(?,?);', array($startDatePie, $endDatePie));
        
        $pieArray=[$totalNonActiveUsers[0]->total_accounts - $totalWithPayments[0]->total_inactive_with_payments, $totalWithPayments[0]->total_inactive_with_payments];
        return response()->json(['type'=>'pie', 'label'=>['Non-active, no payment', 'Non-Active Users With Payment'],'element'=> 'nonActivatedUsersWithPaymentsByPaymentDatePieChart','items'=>$pieArray]);
    }


    public function usersWhoHaveNeverPaidForThemselves(Request $request)
    {
        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_paid_for_themselves_by_create_date(?,?);', array($startDatePie, $endDatePie));
        $totalNeverPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_accounts_never_paid_for_themsleves_by_create_date(?,?);', array($startDatePie, $endDatePie));


        $pieArray=[$totalNeverPaidForThemselves[0]->total_never_paid_for_self, $totalPaidForThemselves[0]->total_paid_for_self];
        return response()->json(['type'=>'pie', 'label'=>['Total Users Never Paid Themselves', 'Total paid for themselves'],'element'=> 'usersWhoHaveNeverPaidForThemselvesPieChart','items'=>$pieArray]);


    }

    public function usersWhoHaveNeverPaidForThemselvesByPaymentDate(Request $request)
    {
        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_paid_for_themselves_by_pay_date(?,?);', array($startDatePie, $endDatePie));
        $totalNeverPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_accounts_never_paid_for_themsleves_by_payment_date(?,?);', array($startDatePie, $endDatePie));


        $pieArray=[$totalNeverPaidForThemselves[0]->total_never_paid_for_self, $totalPaidForThemselves[0]->total_paid_for_self];
        return response()->json(['type'=>'pie', 'label'=>['Total Users Never Paid Themselves', 'Total paid for themselves'],'element'=> 'usersWhoHaveNeverPaidForThemselvesByPaymentDatePieChart','items'=>$pieArray]);


    }

    public function activatedUsersWhoHaveNeverPaidForThemselves(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalActivatedPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_active_users_paid_themselves_by_create_date(?,?);', array($startDatePie, $endDatePie));
        $totalActivatedNeverPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_active_users_never_paid_themselves_by_create_date(?,?);', array($startDatePie, $endDatePie));

        $pieArray=[$totalActivatedNeverPaidForThemselves[0]->total_never_paid_for_self,$totalActivatedPaidForThemselves[0]->total_active_paid_for_self];
        return response()->json(['type'=>'pie', 'label'=>['Activated Users Never Paid Themselves', 'Activated Users paid for themselves'],'element'=> 'activatedUsersWhoHaveNeverPaidForThemselvesPieChart','items'=>$pieArray]);


    }

    public function activatedUsersWhoHaveNeverPaidForThemselvesByPaymentDate(Request $request)
    {

        $startDatePie = $request->input('dateStart');
        $endDatePie = $request->input('dateEnd');

        $totalActivatedPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_active_users_paid_themselves_by_create_date(?,?);', array($startDatePie, $endDatePie));
        $totalActivatedNeverPaidForThemselves = DB::connection($this->db)->select('CALL sp_total_active_users_never_paid_themselves_by_payment_date(?,?);', array($startDatePie, $endDatePie));

        $pieArray=[$totalActivatedNeverPaidForThemselves[0]->total_never_paid_for_self,$totalActivatedPaidForThemselves[0]->total_active_paid_for_self];
        return response()->json(['type'=>'pie', 'label'=>['Activated Users Never Paid Themselves', 'Activated Users paid for themselves'],'element'=> 'activatedUsersWhoHaveNeverPaidForThemselvesByaymentDatePieChart','items'=>$pieArray]);


    }

    

}
