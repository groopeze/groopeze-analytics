<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataParser;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;


class BookingsDataController extends Controller
{
    private $dataParser;
    private $db;

    public function __construct(DataParser $dataParser, Request $request)
    {
        $data = $request->all();
        $this->db = $data['selected_db'];
        $this->dataParser = $dataParser;
        //$this->middleware('auth');
    }

    // public function Index()
    // {
    //  $bookings = App\Models\Booking::first()->get();
    //  //return response()->json(['response'=>'200 OK', 'items'=>$bookings]);
    //  return response()->json(['response'=>'200 OK','info'=> compact($bookings)]);
    // }

    // public function repeatUsers(Request $request)
    // {
        
        
    //  $data = $request->input();
                
    //  $bookings = \App\Models\Booking::where('start_date', '>=', $data['dateStart'] )->where('start_date', '<=', $data['dateEnd'] )->select('id', 'start_date')->get();
    //  //dd($this->bookingDataParser->returnDataPoints($bookings));

    //  $datapoints = $this->bookingDataParser->returnDataPoints($bookings);

    //  return $datapoints->toJson();//response()->json(['response'=>'200 OK', 'items' => $bookings]);//bookingDataParser->returnDataPoints($bookings)]);


    // }
    
    public function bookingsInOut(Request $request)
    {
        $startdate = $request->input('dateStart');
        $enddate = $request->input('dateEnd');

        if ($startdate == null || $enddate ==null) {
            $startdate = "2016-06-01";//date("Y-m-d") - 60;
            $enddate =  "2017-06-20";//date("Y-m-d") - 30;
        }
        
        $created = $this->dataParser->dataSeperatorXAxisAndYAxis(DB::connection($this->db)->select('Call sp_createdBookingQuery(?,?)', array($startdate, $enddate)));//('select DATE(created) as Date, count(id) as Total from users  where Date(created) >= '.$startdate. 
           
        $start = $this->dataParser->dataSeperatorXAxisAndYAxis(DB::connection($this->db)->select('Call sp_startBookingQuery(?,?)', array($startdate, $enddate)));//('select DATE(created) as Date, count(id) as Total from users  where Date(created) >= '.$startdate. 
                  
        // // //the below returns the date activated
        $annoyingArray = [$created, $start];
        $colourArray = ['rgba(75,192,192,0.4)', 'rgba(255,0,0,0.4)'];
        $labelArray = ['Bookings Created', 'Bookings Out'];
        
        return  response()->json(['type'=>'line', 'element'=>'bookingsCreated', 'labels' => $labelArray, 'colours' => $colourArray, 'items' => $annoyingArray]);
        //return  json_encode(['type'=>'line', 'element'=>'usersCreated', 'items' =>$created]);
    }

    public function mostPopularBookingDays(Request $request)
    {
        //get date input from request
        $startdatebooking = $request->input('dateStart');
        $enddatebooking = $request->input('dateEnd');

        //determine if they want months or days
        $dataFormat = ($request->value == null) ? 1 : $request->value;

        //fire request and recieve result
        if ($dataFormat == 1) {
            $mostPopBooking = DB::connection($this->db)->select('Call sp_most_Popular_Booking_Day_By_Date(?,?);', array($startdatebooking, $enddatebooking));
        }elseif($dataFormat == 2){
            $mostPopBooking = DB::connection($this->db)->select('Call sp_most_Popular_Booking_Month_By_Date(?,?);', array($startdatebooking, $enddatebooking));
        }       
        //get relevant info from query
        $dayBooking = collect($mostPopBooking)->pluck("wd");
        $numberBooking = collect($mostPopBooking)->pluck("cnt");
        //arrange for return (putting the label in an array allows for ajax reuse, the data must be placed in an array in order forhte chart library to use it)
        $labelArray = ['Bookings Created'];
        $data = array($dayBooking,$numberBooking);
        return response()->json(['type'=>'bar', 'labels' => $labelArray, 'element'=>'busiestDaysBookingsCreated',
        'items' => $data, 'date'=>[$startdatebooking, $enddatebooking]]);
    }
    
    public function mostPopularStartDays(Request $request)
    {
        //$dataFormat = $request->value;
        $dataFormat = ($request->value == null) ? 1 : $request->value;
        $startDateOut = $request->input('dateStart');
        $endDateOut = $request->input('dateEnd');
        $mostPop = array();
        
        if ($dataFormat == 1) {
            $mostPopStart = DB::connection($this->db)->select('Call sp_most_Popular_Start_Days_By_Date(?,?);', array($startDateOut, $endDateOut));       
        }elseif($dataFormat == 2){          
            $mostPopStart = DB::connection($this->db)->select('Call sp_most_Popular_Start_Months_By_Date(?,?);', array($startDateOut, $endDateOut));
        }

        $dayStart = collect($mostPopStart)->pluck("wd");
        $numberStart = collect($mostPopStart)->pluck("cnt");

        $labelArray = ['Bookings Out'];

        $data = array($dayStart,$numberStart);
        return response()->json(['type'=>'bar', 'labels' => $labelArray, 'date'=> [$startDateOut, $endDateOut],'element'=>'busiestDaysBookingsStart',
        'items' => $data]);
    }

    public function incomeStats(Request $request)
    {
        $totalIncome = DB::connection($this->db)->select('CALL sp_total_income_all_time');
        $totalPayments = DB::connection($this->db)->select('CALL sp_total_payments_all_time');
        $annoyingArray = [$totalIncome, $totalPayments];
        return response()->json(['type'=>'income_stats', 'items'=>$annoyingArray, 'element'=>'totalIncome']);
    }

    public function incomeOverTime(Request $request)
    {
        $income = $this->dataParser->dataSeperatorXAxisAndYAxis(DB::connection($this->db)->select ('SELECT DATE(payments.datetime_paid) as XAxis, format(sum(payments.amount), 2) AS YAxis FROM payments WHERE DATE(payments.datetime_paid) > "0000-00-00" GROUP BY 1;'));
        
        $labelArray = ['Income'];
        $annoyingArray = [$income, ""];
        $colourArray = ['rgba(75,192,192,0.4)', 'rgba(255,0,0,0.4)'];

        return response()->json(['type'=>'income','element'=>'totalIncomeOverTime', 'labels' => $labelArray, 'colours' => $colourArray,  
        'items' => $annoyingArray]);
    }

    public function mostPopularLocations(Request $request)
    {
        $mostPopLocations = DB::connection($this->db)->select('CALL sp_most_popular_locations');
        
        
        return response()->json(['type'=>'mostPopLocation', 'labels' => '', 'element'=>'popLocationTable', 'items'=>$mostPopLocations]);

    }
}
