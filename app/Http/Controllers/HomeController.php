<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function dash()
    {
        return view('dash');
    }

    public function users()
    {
        return view('users');
    }
    
    public function bookings()
    {
        return view('bookings');
    }

    public function payments()
    {
        return view('payments');
    }
}
