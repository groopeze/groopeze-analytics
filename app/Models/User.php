<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class 
 * 
 * @property int $id
 * @property string $f_name
 * @property string $l_name
 * @property string $email
 * @property int $active
 * @property \Carbon\Carbon $created
 * @property string $password
 * @property string $fb_uid
 * @property string $hash
 * @property int $is_superuser
 * @property string $phone_number
 * @property \Carbon\Carbon $datetime_activated
 * @property \Carbon\Carbon $last_login
 * @property int $terms
 * @property int $remarket
 * 
 * @property \Illuminate\Database\Eloquent\Collection $request_confirms
 * @property \Illuminate\Database\Eloquent\Collection $logins
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $refunds
 * @property \Illuminate\Database\Eloquent\Collection $retrievals
 * @property \Illuminate\Database\Eloquent\Collection $rsvps
 * @property \Illuminate\Database\Eloquent\Collection $serialized_sessions
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 *
 * @package App\Models
 */
class User extends Eloquent
{
	public $timestamps = false;

	// protected $casts = [
	// 	'active' => 'int',
	// 	'is_superuser' => 'int',
	// 	'terms' => 'int',
	// 	'remarket' => 'int'
	// ];

	// protected $dates = [
	// 	'created',
	// 	'datetime_activated',
	// 	'last_login'
	// ];

	// protected $hidden = [
	// 	'password'
	// ];

	// protected $fillable = [
	// 	'f_name',
	// 	'l_name',
	// 	'email',
	// 	'active',
	// 	'created',
	// 	'password',
	// 	'fb_uid',
	// 	'hash',
	// 	'is_superuser',
	// 	'phone_number',
	// 	'datetime_activated',
	// 	'last_login',
	// 	'terms',
	// 	'remarket'
	// ];

	public function request_confirms()
	{
		return $this->hasMany(\App\Models\RequestConfirm::class);
	}

	public function logins()
	{
		return $this->hasMany(\App\Models\Login::class, 'u_id');
	}

	public function payments()
	{
		return $this->hasMany(\App\Models\Payment::class, 'u_id');
	}

	public function refunds()
	{
		return $this->hasMany(\App\Models\Refund::class, 'refunded_by');
	}

	public function retrievals()
	{
		return $this->hasMany(\App\Models\Retrieval::class, 'u_id');
	}

	public function rsvps()
	{
		return $this->hasMany(\App\Models\Rsvp::class, 'u_id');
	}

	public function serialized_sessions()
	{
		return $this->hasMany(\App\Models\SerializedSession::class, 'u_id');
	}

	public function bookings()
	{
		return $this->belongsToMany(\App\Models\Booking::class, 'user_to_bookings', 'u_id', 'b_id')
					->withPivot('id', 'paid', 'attending', 'roles', 'tshirt', 'custom_tshirt_text', 'tshirt_number', 'date_paid', 'date_declined', 'transport', 'discount', 'datetime_paid', 'paid_stag_hen', 'flexi_cancel', 'join_token_id');
	}
}
