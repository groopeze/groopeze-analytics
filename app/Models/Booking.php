<?php



namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Booking
 * 
 * @property int $id
 * @property \Carbon\Carbon $date_booked
 * @property float $paid
 * @property \Carbon\Carbon $start_date
 * @property int $num_days
 * @property float $final_cost
 * @property int $num_people
 * @property string $name
 * @property int $group_tshirts
 * @property int $booked
 * @property int $location_id
 * @property int $canceled
 * @property int $package_id
 * @property float $transport
 * @property int $hide
 * @property float $discount
 * @property float $pay_for_stag_hen
 * @property int $vertical_id
 * @property string $itin_status
 * @property int $payments_stopped
 * @property int $supplier_paid_email
 * @property \Carbon\Carbon $deadline
 * 
 * @property \App\Models\Location $location
 * @property \App\Models\Package $package
 * @property \App\Models\Enumeration $enumeration
 * @property \Illuminate\Database\Eloquent\Collection $accommodations
 * @property \Illuminate\Database\Eloquent\Collection $cancel_booking_hashes
 * @property \Illuminate\Database\Eloquent\Collection $day_activities
 * @property \Illuminate\Database\Eloquent\Collection $night_activities
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $rsvps
 * @property \Illuminate\Database\Eloquent\Collection $serialized_sessions
 * @property \Illuminate\Database\Eloquent\Collection $transport_group_payments
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Booking extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'paid' => 'float',
		'num_days' => 'int',
		'final_cost' => 'float',
		'num_people' => 'int',
		'group_tshirts' => 'int',
		'booked' => 'int',
		'location_id' => 'int',
		'canceled' => 'int',
		'package_id' => 'int',
		'transport' => 'float',
		'hide' => 'int',
		'discount' => 'float',
		'pay_for_stag_hen' => 'float',
		'vertical_id' => 'int',
		'payments_stopped' => 'int',
		'supplier_paid_email' => 'int'
	];

	protected $dates = [
		'date_booked',
		'start_date',
		'deadline'
	];

	protected $fillable = [
		'date_booked',
		'paid',
		'start_date',
		'num_days',
		'final_cost',
		'num_people',
		'name',
		'group_tshirts',
		'booked',
		'location_id',
		'canceled',
		'package_id',
		'transport',
		'hide',
		'discount',
		'pay_for_stag_hen',
		'vertical_id',
		'itin_status',
		'payments_stopped',
		'supplier_paid_email',
		'deadline'
	];

	public function location()
	{
		return $this->belongsTo(\App\Models\Location::class);
	}

	public function package()
	{
		return $this->belongsTo(\App\Models\Package::class);
	}

	public function enumeration()
	{
		return $this->belongsTo(\App\Models\Enumeration::class, 'vertical_id');
	}

	public function accommodations()
	{
		return $this->hasMany(\App\Models\Accommodation::class, 'b_id');
	}

	public function cancel_booking_hashes()
	{
		return $this->hasMany(\App\Models\CancelBookingHash::class);
	}

	public function day_activities()
	{
		return $this->hasMany(\App\Models\DayActivity::class, 'b_id');
	}

	public function night_activities()
	{
		return $this->hasMany(\App\Models\NightActivity::class, 'b_id');
	}

	public function payments()
	{
		return $this->hasMany(\App\Models\Payment::class, 'b_id');
	}

	public function rsvps()
	{
		return $this->hasMany(\App\Models\Rsvp::class, 'b_id');
	}

	public function serialized_sessions()
	{
		return $this->hasMany(\App\Models\SerializedSession::class, 'b_id');
	}

	public function transport_group_payments()
	{
		return $this->hasMany(\App\Models\TransportGroupPayment::class);
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'user_to_bookings', 'b_id', 'u_id')
					->withPivot('id', 'paid', 'attending', 'roles', 'tshirt', 'custom_tshirt_text', 'tshirt_number', 'date_paid', 'date_declined', 'transport', 'discount', 'datetime_paid', 'paid_stag_hen', 'flexi_cancel', 'join_token_id');
	}
}
