<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TransportGroupPayment
 * 
 * @property int $id
 * @property int $booking_id
 * @property int $enumeration_id
 * @property int $numbers
 * @property \Carbon\Carbon $datetime_paid
 * @property float $price
 * @property int $payments_id
 * @property int $activity_id
 * @property int $day_activity_id
 * 
 * @property \App\Models\Booking $booking
 * @property \App\Models\Enumeration $enumeration
 * @property \App\Models\Payment $payment
 * @property \App\Models\Activity $activity
 * @property \App\Models\DayActivity $day_activity
 *
 * @package App\Models
 */
class TransportGroupPayment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'booking_id' => 'int',
		'enumeration_id' => 'int',
		'numbers' => 'int',
		'price' => 'float',
		'payments_id' => 'int',
		'activity_id' => 'int',
		'day_activity_id' => 'int'
	];

	protected $dates = [
		'datetime_paid'
	];

	protected $fillable = [
		'booking_id',
		'enumeration_id',
		'numbers',
		'datetime_paid',
		'price',
		'payments_id',
		'activity_id',
		'day_activity_id'
	];

	public function booking()
	{
		return $this->belongsTo(\App\Models\Booking::class);
	}

	public function enumeration()
	{
		return $this->belongsTo(\App\Models\Enumeration::class);
	}

	public function payment()
	{
		return $this->belongsTo(\App\Models\Payment::class, 'payments_id');
	}

	public function activity()
	{
		return $this->belongsTo(\App\Models\Activity::class);
	}

	public function day_activity()
	{
		return $this->belongsTo(\App\Models\DayActivity::class);
	}
}
