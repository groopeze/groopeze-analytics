<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payment
 * 
 * @property int $id
 * @property int $b_id
 * @property \Carbon\Carbon $datetime_paid
 * @property float $amount
 * @property int $paid_by
 * @property int $payment_type
 * @property int $payment_taken_by
 * @property string $SecurityKey
 * @property string $status
 * @property string $TxAuthNo
 * @property int $u_id
 * @property float $surcharge
 * @property string $VPSTxId
 * @property string $vendor_tx
 * 
 * @property \App\Models\Booking $booking
 * @property \App\Models\User $user
 * @property \App\Models\Enumeration $enumeration
 * @property \Illuminate\Database\Eloquent\Collection $accommodation_group_payments
 * @property \Illuminate\Database\Eloquent\Collection $day_activity_group_payments
 * @property \Illuminate\Database\Eloquent\Collection $night_activity_group_payments
 * @property \Illuminate\Database\Eloquent\Collection $refunds
 * @property \Illuminate\Database\Eloquent\Collection $transport_group_payments
 *
 * @package App\Models
 */
class Payment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'b_id' => 'int',
		'amount' => 'float',
		'paid_by' => 'int',
		'payment_type' => 'int',
		'payment_taken_by' => 'int',
		'u_id' => 'int',
		'surcharge' => 'float'
	];

	protected $dates = [
		'datetime_paid'
	];

	protected $fillable = [
		'b_id',
		'datetime_paid',
		'amount',
		'paid_by',
		'payment_type',
		'payment_taken_by',
		'SecurityKey',
		'status',
		'TxAuthNo',
		'u_id',
		'surcharge',
		'VPSTxId',
		'vendor_tx'
	];

	public function booking()
	{
		return $this->belongsTo(\App\Models\Booking::class, 'b_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'u_id');
	}

	public function enumeration()
	{
		return $this->belongsTo(\App\Models\Enumeration::class, 'payment_type');
	}

	public function accommodation_group_payments()
	{
		return $this->hasMany(\App\Models\AccommodationGroupPayment::class, 'payments_id');
	}

	public function day_activity_group_payments()
	{
		return $this->hasMany(\App\Models\DayActivityGroupPayment::class, 'payments_id');
	}

	public function night_activity_group_payments()
	{
		return $this->hasMany(\App\Models\NightActivityGroupPayment::class, 'payments_id');
	}

	public function refunds()
	{
		return $this->hasMany(\App\Models\Refund::class);
	}

	public function transport_group_payments()
	{
		return $this->hasMany(\App\Models\TransportGroupPayment::class, 'payments_id');
	}
}
