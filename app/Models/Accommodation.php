<?php


namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Accommodation
 * 
 * @property int $id
 * @property int $b_id
 * @property \Carbon\Carbon $day
 * @property int $activity_id
 * @property string $type
 * @property int $occupancy_limit
 * @property int $price
 * 
 * @property \App\Models\Booking $booking
 * @property \App\Models\Activity $activity
 *
 * @package App\Models
 */
class Accommodation extends Eloquent
{
	protected $table = 'accommodation';
	public $timestamps = false;
    //no need for casts array
	// protected $casts = [
	// 	'b_id' => 'int',
	// 	'activity_id' => 'int',
	// 	'occupancy_limit' => 'int',
	// 	'price' => 'int'
	// ];

	protected $dates = [
		'day'
	];

	// protected $fillable = [
	// 	'b_id',
	// 	'day',
	// 	'activity_id',
	// 	'type',
	// 	'occupancy_limit',
	// 	'price'
	// ];

	public function booking()
	{
		return $this->belongsTo(\App\Models\Booking::class, 'b_id');
	}

	public function activity()
	{
		return $this->belongsTo(\App\Models\Activity::class);
	}
}
