<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TshirtGroupPayment
 * 
 * @property int $id
 * @property int $booking_id
 * @property int $enumeration_id
 * @property int $numbers
 * @property \Carbon\Carbon $datetime_paid
 * @property float $price
 * @property int $payments_id
 *
 * @package App\Models
 */
class TshirtGroupPayment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'booking_id' => 'int',
		'enumeration_id' => 'int',
		'numbers' => 'int',
		'price' => 'float',
		'payments_id' => 'int'
	];

	protected $dates = [
		'datetime_paid'
	];

	protected $fillable = [
		'booking_id',
		'enumeration_id',
		'numbers',
		'datetime_paid',
		'price',
		'payments_id'
	];
}
