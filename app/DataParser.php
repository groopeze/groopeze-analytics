<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class DataParser extends Model
{
    /**The below function takes in a SLQ query result in array form intended
     *  to be diplayed on a graph wth x and y axis, 
     * the SQL query must return the data aray aliased as XAxis andYAxis */
    public function dataSeperatorXAxisAndYAxis($data)
    {
        $x = collect($data)->pluck("XAxis");
        $y = collect($data)->pluck("YAxis");
        $seperatedCollection = [$x, $y];
        return ($seperatedCollection);
    }

}
