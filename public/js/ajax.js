//urls to iterate through on page load/change
var url = ['/users/createdActivated', '/users/activatedAndNonActivatedAccountsDeclinedATrip', '/bookings/incomeStats', '/users/totalAccountsAndTotalSentOnBooking', '/users/activatedUsersWhoHaveNeverPaidForThemselves', '/users/activatedUsersWhoHaveNeverPaidForThemselvesByPaymentDate', '/users/usersWhoHaveNeverPaidForThemselves', '/users/usersWhoHaveNeverPaidForThemselvesByPaymentDate', '/users/nonActivatedUsersWithPaymentsPieChart', '/users/nonActivatedUsersWithPaymentsByPaymentDatePieChart', '/users/usersWithPaymentsPieChart', '/users/usersWithPaymentsByPaymentDatePieChart', '/users/userPieChart', '/users/activatedUserByPaymentDatePieChart', '/users/activeUserPieChart', '/users/avgSpendPerUser', '/users/repeatUsers', '/users/totalActive', '/users/mostPopularDays', '/bookings/inout', '/bookings/mostPopularBookingDays', '/bookings/mostPopularStartDays', '/bookings/mostPopularLocations'];
// var selected_db = 'thestagsballs';

var data = {
    dateStart: $('input[name=dateStart]').val(),
    dateEnd: $('input[name=dateEnd]').val(),
    selected_db: 'thestagsballs'

}
$(document).ready(function () {

    /* users created/activated 
    *  mostPopular days for user creation
    */
    url.forEach(function (val, index){
        get(val, data, drawChart);
    });

});

// function ajaxRequest(url) {
//     //getting the input from the date picker, then sent as form data to controller
//     var formData = {
//         'dateStart': $('input[name=dateStart]').val(),
//         'dateEnd': $('input[name=dateEnd]').val()
//     };
//     $.ajax({
//         type: 'GET',
//         url: url,
//         data: formData,
//         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
//         success: function (result) {
//             //console.log(result);
//             drawChart(result);
//         },
//         error: function (result) {
//             alert('Oops something went wrong!' + console.log(result));
//         }
//     });
// }

function drawChart(result) {

    var response = result;
    //console.log(response);
    switch (response.type) {
        case 'line':
            drawLineGraph(response);
            break;
        case 'bar':
            drawBarChart(response);
            break;
        case 'info':
            updateinfo(response);
            break;
        case 'table':
            repeatUserTable(response);
            break;
        case 'pie':
            drawPieChart(response);
            break;
        case 'income_stats':
            displayIncome(response);
            break;
        case 'mostPopLocation':
            mostPopLocationTable(response);
            break;
        default:

            //alert('Uh oh, I dont know what kind of chart to draw (line 45)!' + console.log(response));
    }
}
function displayIncome(result) {
    //console.log(result);
    var target = document.getElementById(result.element);
    var noOfPayments = document.getElementById('noOfPayments');
    target.innerHTML = result.items[0][0].total_amount;
    noOfPayments.innerHTML = result.items[1][0].cnt;



}
function incomeChart(result) {
    // console.log(result);
    ctx = document.getElementById(result.element);
    var data = {
        labels: result.items[0][0],
        datasets: [{
            label: "income over time",
            borderColor: "rgba(75,192,192,1)",
            data: result.items[0][1],
        }]

    };
    //console.log(data);
    usersLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            showLines: true,
            spanGaps: true,
        }
    });
}

function drawPieChart(result) {
    console.log(result);
    ctx = document.getElementById(result.element);

    var data = {
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: result.label,
        datasets: [{
            data: result.items,
            backgroundColor: [
                'green',
                'blue',
            ],
            borderColor: ['black', 'black'],
            borderWidth: [2, 2]
        }],                
    };


    switch (ctx.id) {
        case "totalAccountsAndTotalSentOnBookingPieChart":
        
            
            totalAccountsAndTotalSentOnBookingPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    ledgend:{
                        
                    },
                    responsive: true
                }
            });
            break;
        case "activatedAndActivatedDeclinedBooking":
        
            activatedAndActivatedDeclinedBooking = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;

        case "totalUsersVsActivated":
            totalUsersVsActivated = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "totalUsersAndUsersWithPaymentPie":
            totalUsersAndUsersWithPaymentPie = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "totalUsersAndUsersWithPaymentByPaymentDatePie":
            totalUsersAndUsersWithPaymentByPaymentDatePie = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "totalActiveUsersAndUsersWithPaymentPie":
            totalActiveUsersAndUsersWithPaymentPie = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "totalActiveUsersAndUsersWithPaymentByPaymentDatePie":
            totalActiveUsersAndUsersWithPaymentByPaymentDatePie = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "nonActivatedUsersWithPaymentsPieChart":
            nonActivatedUsersWithPaymentsPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "nonActivatedUsersWithPaymentsByPaymentDatePieChart":
            nonActivatedUsersWithPaymentsByPaymentDatePieChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "usersWhoHaveNeverPaidForThemselvesPieChart":
            usersWhoHaveNeverPaidForThemselvesPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "usersWhoHaveNeverPaidForThemselvesByPaymentDatePieChart":
            usersWhoHaveNeverPaidForThemselvesByPaymentDatePieChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "activatedUsersWhoHaveNeverPaidForThemselvesPieChart":
            activatedUsersWhoHaveNeverPaidForThemselvesPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        case "activatedUsersWhoHaveNeverPaidForThemselvesByaymentDatePieChart":
            activatedUsersWhoHaveNeverPaidForThemselvesByaymentDatePieChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: {
                    responsive: true
                }
            });
            break;
        default:
            break;
    }





}
function updateinfo(result) {
    //console.log(result);
    var content = result.items.count;



    var newNode = document.createElement('div');
    for (p in result.items) {

        newNode.innerHTML = result.items[p];

    }
    //console.log(newNode);
    document.getElementById(result.element).appendChild(newNode);

}

function repeatUserTable(result) {
    //console.log(result);



    var $table = $('#userstable');
    $table.bootstrapTable({
        data: result.items,
        search: true,
        pagination: true,
        buttonsClass: 'primary',
        showFooter: true,
        //showHeader: false,
        minimumCountColumns: 2,
        columns: [{
            field: 'First_Name',
            title: 'First Name',
            sortable: true,
        }, {
            field: 'Last_Name',
            title: 'Last Name',
            sortable: true,
        }, {
            field: 'email',
            title: 'Email',
            sortable: true,

        }, {
            field: 'No_Of_Repeats',
            title: 'Count',
            sortable: true,

        }, {
            field: 'total_spend',
            title: 'total spent',
            sortable: true,
        }],


    });
    $repeatusers = result.items;
    //console.log($repeatusers.length);
    document.getElementById('noOfRepeats').innerHTML = $repeatusers.length;
    //the below is a hack to stop the loading message appearing. I think the response object needs a specifc value to let the table know it's
    //finished loading but I will rip this laptop in half if I have to do any more with it
    $('#userstable').bootstrapTable('hideLoading');


}

function mostPopLocationTable(result) {
    //console.log(result);


    document.getElementById('noOfLocations').innerHTML = result.items.length;
    var $table = $('#popLocationTable');
    $table.bootstrapTable({
        data: result.items,
        search: true,
        pagination: true,
        buttonsClass: 'primary',
        showFooter: true,
        //showHeader: false,
        minimumCountColumns: 2,
        columns: [{
            field: 'location',
            title: 'Location',
            sortable: true,
        }, {
            field: 'freq',
            title: 'Times Booked',
            sortable: true,
        },],


    });
    $repeatusers = result.items;
    //console.log($repeatusers.length);
    document.getElementById('noOfRepeats').innerHTML = $repeatusers.length;
    //the below is a hack to stop the loading message appearing. I think the response object needs a specifc value to let the table know it's
    //finished loading but I will rip this laptop in half if I have to do any more with it
    $('#userstable').bootstrapTable('hideLoading');


}

function drawLineGraph(result) {
    //console.log(result);
    //arrange chart

    let $inputDataset = [];
    var $array = [];


    $array = result.items.slice();

    $.each($array, function (index, value) {

        const tempDataset = {
            label: result.labels[index],
            backgroundColor: result.colours[index],
            borderColor: "rgba(75,192,192,1)",
            data: result.items[index][1]
        };
        //console.log(tempDataset);
        //we need to use the .push method to allow for updates
        $inputDataset.push(tempDataset);
    });

    // var data = {
    //     labels: result.items[0][0], //xvalues 
    //     labels: result.items[1][0], //xvalues
    //     datasets: $inputDataset,
    // };
    // zip chart coord data into array of objects of x and y coordinates
    var bookingsZipped = result.items[0][0].map(function (e, i) {
        return { x: e, y: $inputDataset[0].data[i] };
    });

    var usersZipped = result.items[1][0].map(function (e, i) {
        return { x: e, y: $inputDataset[1].data[i] };
    });

    var chartData = {
        datasets: [{
            label: result.labels[0],
            backgroundColor: result.colours[0],
            borderColor: "rgba(75,192,192,1)",
            data: bookingsZipped,
        },
        {
            label: result.labels[1],
            backgroundColor: result.colours[1],
            borderColor: "rgba(75,192,192,1)",
            data: usersZipped,
        },
        ]
    };

    //create chart
    ctx = document.getElementById(result.element);
    //var MyLineChart = result.element;
    //console.log(result.element);
    //console.log(data);
    switch (ctx.id) {
        case 'usersCreated':
            usersLineChart = new Chart(ctx, {
                type: 'scatter',
                data: chartData,
                options: {
                    scales: {
                        xAxes: [{
                            display: true,
                            type: 'time',
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }

            });
            break;
        case 'bookingsCreated':
            bookingLineChart = new Chart(ctx, {
                type: 'scatter',
                data: chartData,
                options: {
                    scales: {
                        xAxes: [{
                            display: true,
                            type: 'time',
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            break;

        default:
            alert('Dem line charts be fucky' + console.log(result.id));
    }
}
//dataset index is the index of the array of data sets (currently three) that is to be drawn
//data is the data returned from the ajax submit thing and chart is the chart.
//TO DO
//decouple it now that it's working. 
function addData(chart, result, datasetIndex) {


    switch (chart) {
        case 'usersCreated':

            var usersCreatedUpdateZipped = result.items[0][0].map(function (e, i) {
                return { x: e, y: result.items[0][1][i] };
            });

            var usersActivatedUpdateZipped = result.items[1][0].map(function (e, i) {
                return { x: e, y: result.items[1][1][i] };
            });

            usersLineChart.data.datasets[0].data = usersCreatedUpdateZipped;
            usersLineChart.data.datasets[1].data = usersActivatedUpdateZipped;
            usersLineChart.data.labels = result.items[0][0];
            usersLineChart.update();
            break;

        case 'bookingsCreated':

            var bookingsCreatedUpdateZipped = result.items[0][0].map(function (e, i) {
                return { x: e, y: result.items[0][1][i] };
            });

            var bookingsOutUpdateZipped = result.items[1][0].map(function (e, i) {
                return { x: e, y: result.items[1][1][i] };
            });

            bookingLineChart.data.datasets[0].data = bookingsCreatedUpdateZipped;
            bookingLineChart.data.datasets[1].data = bookingsOutUpdateZipped;

            bookingLineChart.update();
            break;
    }
}

function drawBarChart(result) {

    //console.log(result)

    //arrange chart
    ctx = document.getElementById(result.element);

    var data = {
        labels: result.items[0],
        datasets: [{
            label: result.labels[0],
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            data: result.items[1]
        }]
    };
    //console.log(ctx);
    switch (ctx.id) {
        case "busiestDaysUsers":
            busiestDaysUsers = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Day'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            break;
        case "busiestDaysBookingsStart":
            busiestDaysStart = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Day'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            break;
        case "busiestDaysBookingsCreated":
            busiestDaysCreated = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Day'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            break;
    }
}



$(".date_form_users").submit(function (event) {
    event.preventDefault();
    var formData = {
        'dateStart': $('input[name=dateStart]').val(),
        'dateEnd': $('input[name=dateEnd]').val(),
        selected_db: 'thestagsballs'
    };
    
    $.ajax({
        type: 'GET',
        url: '/users/createdActivated',
        data: formData,
        dataType: 'json',
        encode: true,

        //on completion
        success: function (data) {
            
            addData(data.element, data, 0);
        },
        error: function (result) {
            //console.log(result);
            alert('Oops update went wrong!' + console.log(result));
        }

    });
    

});


$(".date_form_Users_Created_bar_Chart").submit(function (event) {

    
    var formData = {
        'dateStart': $('input[name=dateStartUserCreatedBar]').val(),
        'dateEnd': $('input[name=dateEndUserCreatedBar]').val(),
        selected_db: 'thestagsballs'
    };
   
    $.ajax({
        type: 'GET',
        url: '/users/mostPopularDays',
        data: formData,
        dataType: 'json',
        encode: true,

        //on completion
        success: function (data) {
            //console.log(data);
            if (busiestDaysUsers) {
                busiestDaysUsers.destroy();
            }
            drawChart(data);
        },
        error: function (result) {
            //console.log(result);
            alert('Oops update went wrong!' + console.log(result));
        }

    });
    event.preventDefault();


   

});

$(".date_form_bookings").submit(function (event) {

    
    var formData = {
        'dateStart': $('input[name=dateStartBookings]').val(),
        'dateEnd': $('input[name=dateEndBookings]').val(),
        selected_db: 'thestagsballs'
    };
    
    $.ajax({
        type: 'GET',
        url: '/bookings/inout',
        data: formData,
        dataType: 'json',
        encode: true,

        //on completion
        success: function (data) {
            
            addData(data.element, data, 0);
        },
        error: function (result) {
            //console.log(result);
            alert('Oops update went wrong!' + console.log(result));
        }

    });
    event.preventDefault();

    //}
    // stop the form from submitting

});

$(".date_form_booking_bar_Chart").submit(function (event) {

    
    var formData = {
        'dateStart': $('input[name=dateStartBookingBar]').val(),
        'dateEnd': $('input[name=dateEndBookingBar]').val(),
        selected_db: 'thestagsballs'
    };
  
    $.ajax({
        type: 'GET',
        url: '/bookings/mostPopularBookingDays',
        data: formData,
        dataType: 'json',
        encode: true,

        //on completion
        success: function (data) {
            
            if (busiestDaysCreated) {
                busiestDaysCreated.destroy();
            }
            drawChart(data);
        },
        error: function (result) {
            //console.log(result);
            alert('Oops update went wrong!' + console.log(result));
        }

    });
    event.preventDefault();


   
});

$(".date_form_booking_out_bar_Chart").submit(function (event) {

    event.preventDefault();
    var formData = {
        'dateStart': $('input[name=dateStartBookingOutBar]').val(),
        'dateEnd': $('input[name=dateEndBookingOutBar]').val(),
        selected_db: 'thestagsballs'
    };
   
    $.ajax({
        type: 'GET',
        url: '/bookings/mostPopularStartDays',
        data: formData,
        dataType: 'json',
        encode: true,

        //on completion
        success: function (data) {
            
            if (busiestDaysStart) {
                busiestDaysStart.destroy();
            }
            drawChart(data);
        },
        error: function (result) {
            //console.log(result);
            alert('Oops update went wrong!' + console.log(result));
        }
    });
});

$(".date_form_pieCharts").submit(function (event) {

    event.preventDefault();


    var formData = {
        'dateStart': $('input[name=dateStartPieCharts]').val(),
        'dateEnd': $('input[name=dateEndPieCharts]').val(),
        selected_db: 'thestagsballs'
    };
    


    var pieChartURLs = ['/users/totalAccountsAndTotalSentOnBooking', '/users/activatedAndNonActivatedAccountsDeclinedATrip', '/users/userPieChart','/users/usersWithPaymentsPieChart', '/users/usersWithPaymentsByPaymentDatePieChart', '/users/activeUserPieChart', '/users/activatedUserByPaymentDatePieChart', '/users/nonActivatedUsersWithPaymentsPieChart', '/users/nonActivatedUsersWithPaymentsByPaymentDatePieChart', '/users/usersWhoHaveNeverPaidForThemselves','/users/usersWhoHaveNeverPaidForThemselvesByPaymentDate', '/users/activatedUsersWhoHaveNeverPaidForThemselves', '/users/activatedUsersWhoHaveNeverPaidForThemselvesByPaymentDate'];
   
    pieChartURLs.forEach(ajaxUpdate);

    // process the form

    function ajaxUpdate(updateURLs) {
        //console.log(updateURLs)
        $.ajax({
            type: 'GET',
            url: updateURLs,
            data: formData,
            dataType: 'json',
            encode: true,

            //on completion
            success: function (data) {
                
                if (data.element) {
                    console.log(data.element);
                }
                
                switch (data.element) {
                    case "totalAccountsAndTotalSentOnBookingPieChart":
                    
                        totalAccountsAndTotalSentOnBookingPieChart.destroy();
                        drawPieChart(data);
                        break;
                    case "activatedAndActivatedDeclinedBooking":
                    
                        activatedAndActivatedDeclinedBooking.destroy();
                        drawPieChart(data);
                        break;
            
                    case "totalUsersVsActivated":
                        totalUsersVsActivated.destroy();
                        drawPieChart(data);
                        break;
                    case "totalUsersAndUsersWithPaymentPie":
                        totalUsersAndUsersWithPaymentPie.destroy();
                        drawPieChart(data);
                        break;
                    case "totalActiveUsersAndUsersWithPaymentPie":
                        totalActiveUsersAndUsersWithPaymentPie.destroy();
                        drawPieChart(data);                     
                        break;
                    case "nonActivatedUsersWithPaymentsPieChart":
                        nonActivatedUsersWithPaymentsPieChart.destroy();
                        drawPieChart(data);
                        break;
                    case "usersWhoHaveNeverPaidForThemselvesPieChart":
                        usersWhoHaveNeverPaidForThemselvesPieChart.destroy();
                        drawPieChart(data);
                        break;
                    case "activatedUsersWhoHaveNeverPaidForThemselvesPieChart":
                        activatedUsersWhoHaveNeverPaidForThemselvesPieChart.destroy();
                        drawPieChart(data);
                        break;
            
                    default:
                        break;
                }
            },
            error: function (result) {
                //console.log(result);
                alert('Oops update went wrong!' + console.log(result));
            }

        });
        event.preventDefault();


    }
    

});
/*possibly fix flickering with this https://github.com/jtblin/angular-chart.js/issues/187 
the problem is that the old chart is not being destroyed and instead the new chart is drawn over it, this 
leads to flickering on hover as thelibrary tries to display tool tips from both the old and new chart*/

$("#userBarRadioBtns").on('change', function (event) {
    var radioData = {
        'value': $('input[name=userBarRadio]:checked').val(),
        'dateStart': $('#dateStartUserCreatedBar').val(),
        'dateEnd': $('#dateEndUserCreatedBar').val(),
        selected_db: 'thestagsballs'
    };
    //console.log(radioData);
    $.ajax({
        type: 'GET',
        url: '/users/mostPopularDays',
        data: radioData,
        success: function (data) {
            //console.log(data);

            if (busiestDaysUsers) {
                busiestDaysUsers.destroy();
            }
            drawChart(data);

        }
    });
    event.preventDefault();
});

$("#bookingBarRadioBtns").on('change', function (event) {
    var radioData = {
        'value': $('input[name=bookingBarRadio]:checked').val(),
        'dateStart': $('#dateStartBookingBar').val(),
        'dateEnd': $('#dateEndBookingBar').val(),
        selected_db: 'thestagsballs'
    };

  
    $.ajax({
        type: 'GET',
        url: '/bookings/mostPopularBookingDays',
        data: radioData,
        success: function (data) {
            
            if (busiestDaysCreated) {
                busiestDaysCreated.destroy();
            }
            drawChart(data);
           
        }
    });
    event.preventDefault();
});

$("#startBarRadioBtns").on('change', function (event) {

    var radioData = {
        'value': $('input[name=startBarRadio]:checked').val(),
        'dateStart': $('#dateStartBookingOutBar').val(),
        'dateEnd': $('#dateEndBookingOutBar').val(),
        selected_db: 'thestagsballs'
    };
    
    // url.forEach(function(val, index){
    //     get(val, raadioData, drawChart);
    // });
    $.ajax({
        type: 'GET',
        url: '/bookings/mostPopularStartDays',
        data: radioData,
        success: function (data) {
           

            if (busiestDaysStart) {
                busiestDaysStart.destroy();
            }
            drawChart(data);
        }
    });
    event.preventDefault();
});




$("body").on('change', 'input[name=dbSwitch]', function (event) {
    event.preventDefault();
    data.selected_db =  $(this).val();
    url.forEach(function(val, index){
        get(val, data, drawChart);
    });
    
});

function get(url, input, success){
    $.ajax({
        type: 'GET',
        url: url,
        data: input, 
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        success: function(data){
            success(data)
        },
        error: function(xhr, status, error){
            console.log(JSON.parse(xhr.responseText)) 
        }
    });
}












