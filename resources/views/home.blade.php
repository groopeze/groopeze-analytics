@extends('layouts.app')

@section('content')

    <div class="row">    
        {{--  @include('partials.sidebar')  --}}
        {{--  <div class="col-md-12">  --}}
            <div class="container">
                <div>
                <h1>Dashboard</h1>
                    
                    {{--  <iframe src="https://dataviewr.shinyapps.io/Dashboard/" frameborder="0" style="border: 2px solid black; width: 100%; height: 2000px;"></iframe>--}}
                    <div class="container">
                        @include('partials.datalayout')
                    </div>
                </div>  

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                </div>
            </div>
        {{--  </div>  --}}
    </div>

@endsection


