@extends('layouts.app')

@section('content')

    <div class="row">    
        {{--  @include('partials.sidebar')  --}}
        {{--  <div class="col-md-12">  --}}
            <div class="panel panel-default">
                <div class="panel-heading">
                
                    
                    @include('partials.paymentsdata')
                
                </div>  

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                </div>
            </div>
        {{--  </div>  --}}
    </div>
    <script src="{{ asset('js/ajax.js') }}">
@endsection


