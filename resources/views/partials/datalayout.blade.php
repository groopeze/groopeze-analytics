{{--  <div class="album text-muted" style="width: 100%">  --}}
	<div class="container" style="max-width:100%; background-color: white;">
			<div class="row">
					<div class="container" >
						@include('partials.paymentInfo')
					</div>
				</div>
		<div class="row">
			<div class="container">
				@include('partials.topInfo')
			</div>
		</div>
		<div class="row">
			<div class="container">
     		 @include('partials.userCharts')
			</div>
		</div>

    <div class="row">
			
      @include('partials.bookingCharts')

		</div>

    <div class="row">
			
      <div class="container" style="max-width:100%; background-color: white;">@include('partials.infoCharts')</div>

		</div>
	</div>
{{--  </div>  --}}

<script src="{{ asset('js/ajax.js') }}">

</script>