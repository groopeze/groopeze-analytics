<div class="row">
	<div class="card col-md-10">
		<h3>Number of Users created/activated by date</h3>
		<canvas id="usersCreated"></canvas>
	</div>

	<div class="col-md-2">
		<form method="GET" class="date_form_users" action="api/users/createdActivated">
			{{--
			<div class="form-group" id="userDateSelect"> --}}

				<label for="dateStart">Start</label>
				<input type="date" class="form-control" name="dateStart" id="dateStart" aria-describedby="helpId" placeholder="#" value="2017-10-01">


				<label for="dateEnd">End</label>
				<input type="date" class="form-control" name="dateEnd" id="dateEnd" aria-describedby="helpId" placeholder="#" value="2017-11-01">

				<button type="submit" class="btn btn-primary" id="userDateSubmit">Submit</button>
				{{-- </div> --}}

		</form>
	</div>


</div>

{{--
<div class="card col-md-3">
	<h1>user pie chart</h1>

</div> --}}

<div class="card col-md-6">
	<canvas id="busiestDaysUsers"></canvas>
	<form action="userBarSwap" method="get">
		<div class="form-check form-check-inline" id="userBarRadioBtns">
			<label class="form-check-label" for="userBarRadio">
				<input class="form-check-input" type="radio" name="userBarRadio" id="userBarDay" value="1" checked="checked"> Display Days
			</label>
			<label class="form-check-label" for="userBarRadio">
				<input class="form-check-input" type="radio" name="userBarRadio" id="userBarMonth" value="2"> Display Months
			</label>
		</div>

	</form>
</div>
<div class="col-md-2">
		<form method="GET" class="date_form_Users_Created_bar_Chart" action="api/users/createdActivated">
			{{--
			<div class="form-group" id="userDateSelect"> --}}
	
				<label for="dateStartUserCreatedBar">Start</label>
				<input type="date" class="form-control" name="dateStartUserCreatedBar" id="dateStartUserCreatedBar" aria-describedby="helpId"
				 placeholder="#" value="2017-01-01">
	
	
				<label for="dateEndUserCreatedBar">End</label>
				<input type="date" class="form-control" name="dateEndUserCreatedBar" id="dateEndUserCreatedBar" aria-describedby="helpId" placeholder="#"
				 value="2017-12-31">
	
				<button type="submit" class="btn btn-primary" id="userDateSubmit">Submit</button>
				{{-- </div> --}}
	
		</form>
	</div>