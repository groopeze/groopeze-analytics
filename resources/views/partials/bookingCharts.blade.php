<div class="row">
	<div class="card col-md-10">
		<canvas id="bookingsCreated"></canvas>
	</div <div class="col-md-2">
	<div class="col-md-2">
		<form method="GET" class="date_form_bookings" action="api/bookings/inout">
			{{--
			<div class="form-group" id="userDateSelect"> --}}

				<label for="dateStartBookings">Start</label>
				<input type="date" class="form-control" name="dateStartBookings" id="dateStartBookings" aria-describedby="helpId" placeholder="#">


				<label for="dateEndBookings">End</label>
				<input type="date" class="form-control" name="dateEndBookings" id="dateEndBookings" aria-describedby="helpId" placeholder="#">

				<button type="submit" class="btn btn-primary" id="userDateSubmitBookings">Submit</button>
				{{-- </div> --}}

		</form>
	</div>
</div>



<div class="row">
	<div class="container">
		<h1>busiest months bookings in</h1>
		<div class="card col-md-5">
			<canvas id="busiestDaysBookingsCreated"></canvas>
			<form action="bookingBarSwap" method="get">
				<div class="form-check form-check-inline" id="bookingBarRadioBtns">
					<label class="form-check-label" for="bookingBarRadio">
						<input class="form-check-input" type="radio" name="bookingBarRadio" id="bookingBarDay" value="1" checked="checked"> Display Days
					</label>
					<label class="form-check-label" for="bookingBarRadio">
						<input class="form-check-input" type="radio" name="bookingBarRadio" id="bookingBarMonth" value="2"> Display Months
					</label>
				</div>
				</form>
					</div>
			<div class="col-md-2">
				<form method="GET" class="date_form_booking_bar_Chart" action="api/users/createdActivated">
						<label for="dateStartBookingBar">Start</label>
					<input type="date" class="form-control" name="dateStartBookingBar" id="dateStartBookingBar" aria-describedby="helpId" placeholder="#"
						value="2017-01-01">
							<label for="dateEndBookingBar">End</label>
					<input type="date" class="form-control" name="dateEndBookingBar" id="dateEndBookingBar" aria-describedby="helpId" placeholder="#"
						value="2017-12-31">
						<button type="submit" class="btn btn-primary" id="userDateSubmit">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="row">
		<div class="container">
			<h1>busiest months bookings out</h1>
			<div class="card col-md-5">
				<canvas id="busiestDaysBookingsStart"></canvas>
				<form action="startBarSwap" method="get">
					<div class="form-check form-check-inline" id="startBarRadioBtns">
						<label class="form-check-label" for="startBarRadio">
							<input class="form-check-input" type="radio" name="startBarRadio" id="startBarDay" value="1" checked="checked"> Display Days
						</label>
						<label class="form-check-label" for="startBarRadio">
							<input class="form-check-input" type="radio" name="startBarRadio" id="startBarMonth" value="2"> Display Months
						</label>
					</div>
					</form>
						</div>
				<div class="col-md-2">
					<form method="GET" class="date_form_booking_out_bar_Chart" action="api/users/createdActivated">
							<label for="dateStartBookingOutBar">Start</label>
						<input type="date" class="form-control" name="dateStartBookingOutBar" id="dateStartBookingOutBar" aria-describedby="helpId" placeholder="#"
							value="2017-01-01">
								<label for="dateEndBookingOutBar">End</label>
						<input type="date" class="form-control" name="dateEndBookingOutBar" id="dateEndBookingOutBar" aria-describedby="helpId" placeholder="#"
							value="2017-12-31">
							<button type="submit" class="btn btn-primary" id="userDateSubmit">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>


