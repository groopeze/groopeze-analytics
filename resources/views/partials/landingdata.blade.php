<div class="container">
    <h1>Key metrics</h1>
    <div class="row">
        <div class="col-md-4">
 
                <h2>Total Active Users:</h2>
                <p id="noActiveUsers"><p>
                <h2>Total Payments Recieved:</h2>
                <p id="noOfPayments"></p> 
                <h2>Total Income:</h2>
                <p id="totalIncome"></p>
                
        </div> 
        <div class="col-md-4">
            <label>
                 <span>Incoming bookings past</span>
                <select id="incomingBookingsSelect">
                    <option>Week</option>
                    <option>Month</option>
                </select>
            </label>    
        </div>   
        <div class="card col-md-4">
            
            <h4>Total Users vs Users gone on stags</h4>
            <canvas id="totalAccountsAndTotalSentOnBookingPieChart"></canvas>

        </div>
    </div>
</div>