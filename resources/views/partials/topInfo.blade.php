<div class="col-md-3">
	<div class="card">
		<div class="card-block">
			<h4 class="card-title">Total number of verified users to date:</h4>
			<h4 class="card-text" id="noActiveUsers"></h4>
			
		</div>
	</div>
</div>
<div class="col-md-3">
		<div class="card">
			<div class="card-block">
				<h4 class="card-title">Average spend per user: </h4>
				<h4 class="card-text" id="avgUserSpend">€</h4>
				
			</div>
		</div>
	</div>
<div class="col-md-3">
	<div id="upComingBookings"></div>
</div>
<div class="col-md-3">
	<div id="totalBookings"></div>
</div>