<div class="row" style="max-width: 100%; padding: 10px;">
        
    <div class="col-md-2">
            <form method="GET" class="date_form_pieCharts" action="#">
                <label for="dateStartPieCharts">Start</label>
                <input type="date" class="form-control" name="dateStartPieCharts" id="dateStartPieCharts" aria-describedby="helpId" placeholder="#" value="2013-12-16">
                <label for="dateEndPieCharts">End</label>
                <input type="date" class="form-control" name="dateEndPieCharts" id="dateEndPieCharts" aria-describedby="helpId" placeholder="#" value="2017-12-31">
                <button type="submit" class="btn btn-primary" id="userDateSubmitPieCharts">Submit</button>
            </form>
        </div>

    <div class="col-md-3">
        <h4>Total Users vs Users gone on stags(By booking start date)</h4>
        <canvas id="totalAccountsAndTotalSentOnBookingPieChart"></canvas>
    </div>
    <div class="col-md-3">
        <h4>Total users declining bookings(By booking start date)</h4>
        <canvas id="activatedAndActivatedDeclinedBooking"></canvas>
    </div>
</div>
<div class="row" style="margin:20px;">
    
    <div class="col-md-3">
        <h4>Total Users Not Activated/Activated(By account creation date)</h4>
        <canvas id="totalUsersVsActivated"></canvas>
    </div>
    <div class="col-md-3">
        <h4>Created Users With Payment Made(By account creation date)</h4>
        <canvas id="totalUsersAndUsersWithPaymentPie"></canvas>
    </div>
    <div class="col-md-3">
        <h4>Created Users With Payment Made(By payment date)</h4>
        <canvas id="totalUsersAndUsersWithPaymentByPaymentDatePie"></canvas>
    </div>

</div>
<div class="row" style="margin:20px;">
    <div class="col-md-3">
        <h4>Activated Users With Payment Made(By account activation date)</h4>
        <canvas id="totalActiveUsersAndUsersWithPaymentPie"></canvas>
    </div>

    <div class="col-md-3">
        <h4>Activated Users With Payment Made(By payment date)</h4>
        <canvas id="totalActiveUsersAndUsersWithPaymentByPaymentDatePie"></canvas>
    </div>
    
    <div class="col-md-3">
        <h4>Non-active users and payments made (By account creation date)</h4>
        <canvas id="nonActivatedUsersWithPaymentsPieChart"></canvas>
    </div>
    <div class="col-md-3">
            <h4>Non-active users and payments made (By payment date)</h4>
            <canvas id="nonActivatedUsersWithPaymentsByPaymentDatePieChart"></canvas>
        </div>
</div>
<div class="row" style="margin:20px;">
    

    <div class="col-md-3">
        <h4>Total Users who have a payment made on their account (By account creation date)</h4>
        <canvas id="usersWhoHaveNeverPaidForThemselvesPieChart"></canvas>
    </div>

    <div class="col-md-3">
        <h4>Total Users who have a payment made on their account (By payment date)</h4>
        <canvas id="usersWhoHaveNeverPaidForThemselvesByPaymentDatePieChart"></canvas>
    </div>
</div>
 <div class="row" style="margin:20px;">   
    <div class="col-md-3">
        <h4>Activated Users who have a payment made on their account(By account creation date)</h4>
        <canvas id="activatedUsersWhoHaveNeverPaidForThemselvesPieChart"></canvas>
    </div>

    <div class="col-md-3">
        <h4>Activated Users who have a payment made on their account(By payment date)</h4>
        <canvas id="activatedUsersWhoHaveNeverPaidForThemselvesByaymentDatePieChart"></canvas>
    </div>
</div>

    
    

    
    

