<div class="container">

<div class="row">
    <div class="col-md-4">
        <h4>Created Users With Payment Made</h4>
        <canvas id="totalUsersAndUsersWithPaymentPie"></canvas>
    </div>
    <div class="col-md-4">
        <h4>Activated Users With Payment Made</h4>
        <canvas id="totalActiveUsersAndUsersWithPaymentPie"></canvas>
    </div>
    <div class="col-md-4">
        <h4>Non-Activated Users With Payment Made</h4>
        <canvas id="nonActivatedUsersWithPaymentsPieChart"></canvas>
    </div>
    <div class="col-md-4">
        <h4>Created Users who have never paid for themselves</h4>
        <canvas id="usersWhoHaveNeverPaidForThemselvesPieChart"></canvas>
    </div>
    <div class="col-md-4">
        <h4>Activated Users who have never paid for themselves</h4>
        <canvas id="activatedUsersWhoHaveNeverPaidForThemselvesPieChart"></canvas>
    </div>
</div>

</div>